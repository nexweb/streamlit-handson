import streamlit as st  
import time 
import numpy as np 
from sklearn.linear_model import LinearRegression 

st.title("Caching demonstration")

btn = st.button("Test cache")

if btn:
  st.write("Clicked button")

st.subheader("st.cache.data")

@st.cache_data
def cache_this_function():
  time.sleep(5)
  out = "I'm done running"
  
  return out 

out = cache_this_function()

st.write(out)

st.subheader("st.cahce_resource") 

@st.cache_resource
def create_simple_linear_regression():
  time.sleep(2)
  x = np.array([1,2,3,4,5,6,7]).reshape(-1,1)
  y = np.array([1,2,3,4,5,6,7])
  print("x:" , x, "y:", y)
  
  model = LinearRegression().fit(x,y)
  
  return model 

lr = create_simple_linear_regression()
x_prod = np.array([8]).reshape(-1,1)

print("x_prod:", x_prod)
prod = lr.predict(x_prod)

st.write(f"The prediction is:{prod[0]}")
