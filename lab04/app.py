import streamlit as st
from hugchat import hugchat
from hugchat.login import Login


st.title("ChatGPT-like clone")

# Function for generating LLM response
def generate_response(prompt_input, email, passwd):
    # Hugging Face Login
    sign = Login(email, passwd)
    cookies = sign.login()
    # Create ChatBot                        
    chatbot = hugchat.ChatBot(cookies=cookies.get_dict())
    return chatbot.chat(prompt_input)

with st.sidebar:
  hf_email = st.text_input('Enter E-mail:', type='password')
  hf_pass = st.text_input('Enter password:', type='password')
  if not (hf_email and hf_pass):
      st.warning('Please enter your credentials!', icon='⚠️')
  else:
      st.success('Proceed to entering your prompt message!', icon='👉')
    
if "messages" not in st.session_state:
    st.session_state.messages = []

for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.markdown(message["content"])

if prompt := st.chat_input("What is up?"):
    st.session_state.messages.append({"role": "user", "content": prompt})
    with st.chat_message("user"):
        st.markdown(prompt)

    with st.chat_message("assistant"):
        with st.spinner("Thinking..."):
            response = generate_response(prompt, hf_email, hf_pass) 
            st.write(response) 
    st.session_state.messages.append({"role": "assistant", "content": response})