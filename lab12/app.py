# pip install seaborn 
import streamlit as st
import matplotlib.pyplot as plt 
import pandas as pd 

st.header("matplotlib and searborn visualization in streamit")
df = pd.read_csv('./tips.csv')

st.dataframe(df.head())

st.markdown('...')
st.write('1. Find number of Male and Female distribution')
value_counts = df['sex'].value_counts()

st.dataframe(value_counts)

st.markdown("---")
with st.container():
  st.write('1. Find number of Male and Female distrubution (pie and bar)')
  value_counts = df['sex'].value_counts()
  st.write("Index", value_counts.index)
  col1, col2 = st.columns(2)
  
  with col1:
    st.subheader("Pie Chart")
		# draw pie chart
    fig,ax = plt.subplots()
    ax.pie(value_counts, autopct = '%0.2f%%', labels=['Male', 'Female'])
    st.pyplot(fig)
  
  with col2:
    st.subheader("Bar Chart")
		# draw bar chart
    fig,ax = plt.subplots()
    ax.bar(value_counts.index, value_counts)
    st.pyplot(fig)
  
  with st.expander("See explanation"):
    
		# put this in expander
    st.dataframe(value_counts)
         
        