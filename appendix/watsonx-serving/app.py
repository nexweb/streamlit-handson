import streamlit as st
import requests, json

st.title("IBM watsonx.ai Prompt Lab!")

# Function for call serving of LLM 

def watsonx_ai_api(prompts, api_url, api_key):
    API_KEY = api_key
    token_response = requests.post('https://iam.cloud.ibm.com/identity/token', data={"apikey":
    API_KEY, "grant_type": 'urn:ibm:params:oauth:grant-type:apikey'})
    mltoken = token_response.json()["access_token"]

    header = {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + mltoken}
    print("token:", mltoken)
    # NOTE: manually define and pass the array(s) of values to be scored in the next line
    prompt_payload = {"parameters": { "prompt_variables": { "input": prompts } }}

    print('prompt_payload:', prompt_payload)
    response = requests.post(api_url, 
                             json=prompt_payload, 
                             headers={'Authorization': 'Bearer ' + mltoken})

    res= response.json()
    result = res.get('results')
    generated_text = result[0]['generated_text']
    print("generated_text:", generated_text)
  
    return generated_text

with st.sidebar:
    if ('API_KEY' in st.secrets) and ('API_URL' in st.secrets):
        st.success('Credentials already provided!', icon='✅')
        watsonx_api_key = st.secrets['API_KEY']
        watsonx_api_url = st.secrets['API_URL']
    else:
        watsonx_api_key = st.text_input('Enter API Key:')
        watsonx_api_url = st.text_input('Enter API Url:')
  
    if not (watsonx_api_key and watsonx_api_url):
        st.warning('Please enter your credentials!', icon='⚠️')
    else:
        st.success('Proceed to entering your prompt message!', icon='👉')
   
if "messages" not in st.session_state:
    st.session_state.messages = []

for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.markdown(message["content"])

if prompt := st.chat_input("What is up?"):
    st.session_state.messages.append({"role": "user", "content": prompt})
    with st.chat_message("user"):
        st.markdown(prompt)

    with st.chat_message("assistant"):
        with st.spinner("Thinking..."):
            response = watsonx_ai_api(prompt, watsonx_api_url, watsonx_api_key) 
            st.write(response) 
    st.session_state.messages.append({"role": "assistant", "content": response})