import streamlit as st
from dotenv import load_dotenv
from genai.client import Client
from genai.credentials import Credentials
from genai.schema import (
    TextGenerationParameters,
    TextGenerationReturnOptions,
)

#config Watsonx.ai environment
load_dotenv()

credentials = Credentials.from_env()

print(credentials)

client = Client(credentials=credentials)

st.title("IBM watsonx.ai Prompt Lab!")

# Function for generating LLM response

def generate_response(prompts,
                    model_id="meta-llama/llama-2-70b",
                    decoding_method="greedy",
                    top_k = None,
                    top_p = None,
                    max_new_tokens=100,
                    min_new_tokens=30,
                    temperature=1.0,
                    repetition_penalty=1.0,
                    stop_sequences=''
                    ):
    
    if stop_sequences:        
        for response in client.text.generation.create(
            model_id=model_id,
            inputs=prompts,
            parameters=TextGenerationParameters(
                decoding_method=decoding_method,
                max_new_tokens=max_new_tokens,
                min_new_tokens=min_new_tokens,
                return_options=TextGenerationReturnOptions(
                    input_text=True,
                ),
                temperature=temperature,
                repetition_penalty=repetition_penalty,
                random_seed=3293482354,
                stop_sequences=[stop_sequences]
            ),
        ):
            result = response.results[0] 
            print("response result:", result)
    else:
        for response in client.text.generation.create(
            model_id=model_id,
            inputs=prompts,
            parameters=TextGenerationParameters(
                decoding_method=decoding_method,
                top_k = top_k,
                top_p = top_p,
                max_new_tokens=max_new_tokens,
                min_new_tokens=min_new_tokens,
                return_options=TextGenerationReturnOptions(
                    input_text=True,
                ),
                temperature=temperature,
                repetition_penalty=repetition_penalty,
                random_seed=3293482354
            ),
        ):
            result = response.results[0]
             
    return result.generated_text


with st.sidebar:
    if ('GENAI_KEY' in st.secrets) and ('GENAI_API' in st.secrets):
        st.success('Credentials already provided!', icon='✅')
        watsonx_genapi_key = st.secrets['GENAI_KEY']
        watsonx_genapi_url = st.secrets['GENAI_API']
    else:
        watsonx_genapi_key = st.text_input('Enter API Key:')
        watsonx_genapi_url = st.text_input('Enter API Url:')
  
    if not (watsonx_genapi_key and watsonx_genapi_url):
        st.warning('Please enter your credentials!', icon='⚠️')
    else:
        st.success('Proceed to entering your prompt message!', icon='👉')
    st.subheader("LLM Model Parameters")
    model_id = st.selectbox("Model List", options=["meta-llama/llama-3-70b-instruct", "meta-llama/llama-2-13b-chat", "meta-llama/llama-2-70b", "ibm/granite-13b-instruct-v2", "google/flan-ul2", "google/flan-t5-xxl", "codellama/codellama-34b-instruct"], index=0, key="model_id")
    decoding_col1, decoding_col2 = st.columns([2,8])
  
    with decoding_col1:
        st.markdown("Greedy")

    with decoding_col2:
        sampling_on = st.toggle("Sampling")
 
  
    print("decoding_method:", sampling_on)    
    if sampling_on == True:
        top_k = st.slider("Top k:", min_value=0, max_value=100, value=50)
        top_p = st.number_input("Top P:", value=1,  min_value=0, max_value=1)
        decoding_method = "sample"
    else:
        decoding_method = "greedy" 
        top_p = None
        top_k = None 
     
    max_new_tokens = st.number_input("Max New Token:", min_value=10, max_value=512, value=10)   
    min_new_tokens = st.number_input("Min New Token:", min_value=0, max_value=512, value=0) 
    temperature = st.number_input("Temperature:", value=0.0, step=.1, format="%.2f", max_value=2.1, min_value=0.0)
    repetition_penalty = st.number_input("Repetition Penalty",value=1.0, step=.1, format="%.1f", max_value=2.1, min_value=0.0) 
    stop_sequences = st.text_input("Stop sequences: Example:")
    print("top_p:", top_p)
      
if "messages" not in st.session_state:
    st.session_state.messages = []

for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.markdown(message["content"])


if prompt := st.chat_input("What is up?"):
    st.session_state.messages.append({"role": "user", "content": prompt})
    with st.chat_message("user"):

        html_code = f"""
<pre>
{prompt}
</pre>
"""

        st.markdown(prompt)

    with st.chat_message("assistant"):
        with st.spinner("Thinking..."):
            response = generate_response(prompt, 
                                        model_id, 
                                        decoding_method, 
                                        top_k,
                                        top_p,
                                        max_new_tokens, 
                                        min_new_tokens,
                                        temperature,
                                        repetition_penalty,
                                        stop_sequences) 
            st.write(response) 
    st.session_state.messages.append({"role": "assistant", "content": response})